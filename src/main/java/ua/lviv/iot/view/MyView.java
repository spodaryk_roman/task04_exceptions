package ua.lviv.iot.view;

import ua.lviv.iot.controller.PersonControllerImpl;
import ua.lviv.iot.exceptions.AgeLessZeroException;
import ua.lviv.iot.exceptions.PersonNotExistException;
import ua.lviv.iot.models.Person;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private PersonControllerImpl controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Person person;


    public MyView() {
        person = null;

        controller = new PersonControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - create new person");
        menu.put("2", "  2 - show exist person");
        menu.put("3", "  3 - set person name");
        menu.put("4", "  4 - set person age");
        menu.put("5", "  5 - set person eye color");
        menu.put("6", "  6 - print person data to file");
        menu.put("7", "  7 - delete person");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
    }

    private void pressButton1() {
        System.out.println("print eye color: ");
        String eye = input.next();
        System.out.println("print age color: ");
        int age = input.nextInt();
        System.out.println("print name: ");
        String name = input.next();
        person = new Person();
        person.setAge(age);
        person.setEyeColor(eye);
        person.setName(name);
    }

    private void pressButton2() throws PersonNotExistException{
        controller.getPersonInfo(person);
    }

    private void pressButton3() {
        System.out.println("print name color: ");
        String name = input.next();
        person.setName(name);
    }

    private void pressButton4() {
        System.out.println("print age color: ");
        int age = input.nextInt();
        person.setAge(age);
    }

    private void pressButton5() {
        System.out.println("print eye color: ");
        String eye = input.next();
        person.setEyeColor(eye);
    }

    private void pressButton6() {
        controller.printPersonInfoToFile(person);
    }

    private void pressButton7() {
        person = null;
    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.\n\n");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
