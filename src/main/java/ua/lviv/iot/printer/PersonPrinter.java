package ua.lviv.iot.printer;

import ua.lviv.iot.exceptions.WrongWriteException;
import ua.lviv.iot.models.Person;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public class PersonPrinter implements AutoCloseable {

    private PrintWriter printWriter;

    public PersonPrinter() throws IOException {
        printWriter = new PrintWriter("person-info", StandardCharsets.UTF_8);
    }

    public void writeInfoToFile(Person person) {
        printWriter.println("Age: " + person.getAge());
        printWriter.println("Name: " + person.getName());
        printWriter.println("EyeColor: " + person.getEyeColor());
    }

    @Override
    public void close() throws WrongWriteException {
        if (!printWriter.checkError()) {
            printWriter.close();
        } else {
            throw new WrongWriteException();
        }
    }
}
