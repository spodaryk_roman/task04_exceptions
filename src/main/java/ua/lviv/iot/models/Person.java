package ua.lviv.iot.models;

import ua.lviv.iot.exceptions.AgeLessZeroException;

public class Person {
    private String name;
    private int age;
    private String eyeColor;

    public Person() {
    }

    public Person(String name, int age, String eyeColor) {
        this.name = name;
        this.age = age;
        this.eyeColor = eyeColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws AgeLessZeroException {
        if (age < 0) {
            throw new AgeLessZeroException("AgeLessZeroException");
        } else {
            this.age = age;
        }

    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", eyeColor='" + eyeColor + '\'' +
                '}';
    }
}
