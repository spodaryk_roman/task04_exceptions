package ua.lviv.iot.controller;

import ua.lviv.iot.models.Person;

import java.io.IOException;

public interface PersonController {
    int getPersonAge(Person person);
    String getPersonName(Person person);
    String getPersonEyeColor(Person person);
    void printPersonInfoToFile(Person person) throws IOException;
    void getPersonInfo(Person person);
}
