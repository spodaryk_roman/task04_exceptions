package ua.lviv.iot.controller;

import ua.lviv.iot.exceptions.PersonNotExistException;
import ua.lviv.iot.exceptions.WrongWriteException;
import ua.lviv.iot.models.Person;
import ua.lviv.iot.printer.PersonPrinter;

import java.io.IOException;

public class PersonControllerImpl implements PersonController{

    @Override
    public int getPersonAge(Person person) {
        return person.getAge();
    }

    @Override
    public String getPersonName(Person person) {
        return person.getName();
    }

    @Override
    public String getPersonEyeColor(Person person) {
        return person.getEyeColor();
    }

    @Override
    public void printPersonInfoToFile(Person person) {
        try (PersonPrinter personPrinter = new PersonPrinter()) {
            personPrinter.writeInfoToFile(person);
        } catch (WrongWriteException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getPersonInfo(Person person) throws PersonNotExistException {
        if (person == null) {
            throw new PersonNotExistException("person in not created now");
        }
        System.out.println(person);
    }
}
