package ua.lviv.iot.exceptions;

import java.io.IOException;

public class WrongWriteException extends IOException {
    public WrongWriteException() {
        super();
    }

    public WrongWriteException(String message) {
        super(message);
    }

    public WrongWriteException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongWriteException(Throwable cause) {
        super(cause);
    }
}
