package ua.lviv.iot.exceptions;

public class AgeLessZeroException extends RuntimeException {
    public AgeLessZeroException() {
        super();
    }

    public AgeLessZeroException(String message) {
        super(message);
        System.out.println(message);
    }

    public AgeLessZeroException(String message, Throwable cause) {
        super(message, cause);
    }

    public AgeLessZeroException(Throwable cause) {
        super(cause);
    }
}
