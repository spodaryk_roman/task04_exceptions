package ua.lviv.iot.exceptions;

public class PersonNotExistException extends RuntimeException {
    public PersonNotExistException() {
        super();
    }

    public PersonNotExistException(String message) {
        super(message);
        System.out.println(message);
    }

    public PersonNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersonNotExistException(Throwable cause) {
        super(cause);
    }
}
